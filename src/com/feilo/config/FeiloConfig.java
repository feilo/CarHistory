package com.feilo.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.feilo.interceptor.AuthInterceptor;
import com.feilo.model.Account;
import com.feilo.model.Devinfo;
import com.feilo.model.GpsData;
import com.feilo.model.MileAge;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class FeiloConfig extends JFinalConfig {
	public void configConstant(Constants me) {
		me.setDevMode(true);
		//me.setViewType(ViewType.JSP);
	}

	public void configRoute(Routes me) {
		me.add(new FeiloRoutes()); // 路由
	}

	public void configPlugin(Plugins me) {
		C3p0Plugin cp = new C3p0Plugin(getDBString("url"), getDBString("username"), getDBString("pwd"));
		me.add(cp);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(cp);
		me.add(arp);
		arp.addMapping("account", Account.class);
		arp.addMapping("gpsdata", GpsData.class);
		arp.addMapping("mileage", MileAge.class);
		arp.addMapping("devinfo", Devinfo.class);
		System.out.println("confgi over");
	}

	public void configInterceptor(Interceptors me) {
		me.add(new AuthInterceptor());
	}

	public void configHandler(Handlers me) {
		//me.add(new ResourceHandler());
	}
	
	private static String getDBString(String key) {
		InputStream inputStream = FeiloConfig.class.getResourceAsStream("/db.properties");
		Properties p = new Properties();
		try {
			p.load(inputStream);
			inputStream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return p.getProperty(key);
	}
}