package com.feilo.config;

import com.feilo.controller.AccountController;
import com.feilo.controller.DevinfoController;
import com.feilo.controller.GpsDataController;
import com.feilo.controller.MileAgeController;
import com.jfinal.config.Routes;

public class FeiloRoutes extends Routes {

	@Override
	public void config() {
		add("/gpsdata", GpsDataController.class);
		add("/devinfo", DevinfoController.class);
		add("/account", AccountController.class);
		add("/mileage", MileAgeController.class);
	}

}
