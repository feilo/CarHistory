package com.feilo.test;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Date;

import com.feilo.service.DevinfoService;
import com.feilo.service.DistanceService;
import com.feilo.util.PropertiesUtil;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class TestCalDistance {

	/**
	 * @param args
	 * @throws UnsupportedEncodingException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, ParseException {
		String url = PropertiesUtil.getDBString("url");
		String username = PropertiesUtil.getDBString("username");
		String pwd = PropertiesUtil.getDBString("pwd");
		String ak = PropertiesUtil.getDBString("ak");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(url,username, pwd);
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.start();
		//lzgdj/lzgdj4973759/868120120370124;lzgdgs/lzgdgs5693000	
		String account = "lzgdj";
		String[] imeis = DevinfoService.getImeisFromDevinfo(account);
		for(String imei : imeis){
			System.out.println("=====设备"+imei+":8月所跑的距离（米）======");
			for (int i = 1; i < 32; i++) {
				String begin_fomat = (i > 9) ? (i + "/09/2015 00:00:00") : ("0" + i + "/09/2015 00:00:00");
				String end_fomat = (i > 9) ? (i + "/09/2015 23:59:59") : ("0" + i + "/09/2015 23:59:59");
				long begin_time = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(begin_fomat).getTime();
				long end_time = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(end_fomat).getTime();
				String[] waypoint = DistanceService.getWayPoint(imei, begin_time / 1000, end_time / 1000);
				String[] urls = DistanceService.getDistanceURLs(waypoint,ak);
				float[] distances = DistanceService.getDistanceData(urls);
				float total = 0;
				for (float d : distances) {
					total += d;
				}
				Date d = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(begin_fomat);
				//DistanceService.insertDistance(imei, d, total);
				System.out.println(d+"所跑距离："+total);
			}
		}
	}

}
