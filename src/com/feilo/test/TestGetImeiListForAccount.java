package com.feilo.test;

import org.json.JSONArray;
import org.json.JSONObject;

import com.feilo.service.DevinfoService;
import com.feilo.util.PropertiesUtil;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class TestGetImeiListForAccount {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String url = PropertiesUtil.getDBString("url");
		String username = PropertiesUtil.getDBString("username");
		String upwd = PropertiesUtil.getDBString("pwd");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(url,username, upwd);
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.start();
		String account = "lzgdgs";
		String pwd = "lzgdgs5693000";
		String imeisData = DevinfoService.getDevListForAccount(account,pwd);
		JSONObject jsonObject = new JSONObject(imeisData);
	    JSONArray data =jsonObject.getJSONArray("data");
	    //直接保存的数据库
	    //DevinfoService.saveDevinfo(account,data);
	    //更新设备表，如果设备已存在，则更新属性，如果不存在，增加设备
	    DevinfoService.updateDevinfo(account,data);
		System.out.println(imeisData);
	}

}
