package com.feilo.test;

import java.text.ParseException;

import com.feilo.service.DevinfoService;
import com.feilo.service.GpsHistoryDataService;
import com.feilo.util.PropertiesUtil;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class TestInitHistoryData {


	public static void main(String[] args) throws ParseException {
		String url = PropertiesUtil.getDBString("url");
		String username = PropertiesUtil.getDBString("username");
		String upwd = PropertiesUtil.getDBString("pwd");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(url,username, upwd);
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.start();
		//lzgdj/lzgdj4973759;lzgdgs/lzgdgs5693000	
		String account = "lzgdj";
		String pwd = "lzgdj4973759";
		//String imei = "868120120370124";
		String[] imeis = DevinfoService.getImeisFromDevinfo(account);
		long time = System.currentTimeMillis()/1000;	
		long begin_time = new java.text.SimpleDateFormat ("dd/MM/yyyy HH:mm:ss").parse("01/08/2015 00:00:00").getTime();
		long end_time = new java.text.SimpleDateFormat ("dd/MM/yyyy HH:mm:ss").parse("01/09/2015 00:00:00").getTime();
		String access_token = GpsHistoryDataService.getToken(account,pwd);
		GpsHistoryDataService.doGpsHistoryDataWithToken(access_token, account, time, imeis, "BAIDU", begin_time/1000, end_time/1000);
	    
	}

}
