package com.feilo.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

public class TestMD5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		String url = PropertiesUtil.getDBString("url");
		String username = PropertiesUtil.getDBString("username");
		String upwd = PropertiesUtil.getDBString("pwd");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(url,username, upwd);
		c3p0Plugin.start();
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.start();
		long endtime = GpsHistoryDataService.getEndTime("868120120370124");
		System.out.println(endtime);
		*/
		long t = System.currentTimeMillis();
		System.out.println(t);
		long tt = t - 2629743 ;
		System.out.println(tt);
		System.out.println(getDateStr(t));
		System.out.println(getDateStr(tt));
		
		Date date = new Date();//当前日期
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");//格式化对象
		Calendar calendar = Calendar.getInstance();//日历对象
		calendar.setTime(date);//设置当前日期
		calendar.add(Calendar.MONTH, -3);//月份减一
		String format = sdf.format(calendar.getTime());
		System.out.println(format);//输出格式化的日期
		try {
			System.out.println(sdf.parse(format).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String getDateStr(long millis) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(millis);
		Formatter ft=new Formatter(Locale.CHINA);
		String date = ft.format("%1$tY年%1$tm月%1$td日%1$tA，%1$tT %1$tp", cal).toString();
		ft.close() ;
		return date;
	}

}
