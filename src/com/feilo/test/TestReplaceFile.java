package com.feilo.test;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.feilo.util.BaseUtil;
import com.feilo.util.TextFileUtil;

public class TestReplaceFile {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException {
		String filePath = TestReplaceFile.class.getClass().getResource("/").getPath()+"imei.json";
		String str = BaseUtil.replaceBlank(TextFileUtil.read(filePath));
		String account = "lzgdj";
		String pwd = "lzgdj4973759";
		String imei = "868120120370124,xxx,aaa,dd" ;
		String[] imeis = imei.split(",");
		StringBuffer bimei = new StringBuffer() ;
		String jimei = "" ;
		//判断account是否在字符串中
		int index = str.indexOf(account);
		if(index>=0){
			//把imei加入用户所对应的imei
			//取出json串中的imei数组
			JSONObject jsonObject = new JSONObject(str);
			JSONArray data =jsonObject.getJSONArray("data");
			for (int i = 0; i < data.length(); i++) {
				JSONObject jo = data.getJSONObject(i);
				if(jo.get("account").toString().equals(account)){
					jimei = jo.get("imei").toString();
					bimei.append(jimei);
					break ;
				}
			}
			for(String s : imeis){
				if(!jimei.contains(s)){
					bimei.append(",").append(s);
				}
			}
			str = str.replaceAll(jimei, bimei.toString());
		}else{
			String son = ",{\"account\":\""+account+"\"," + "\"pwd\":\""+pwd+"\"," + "\"imei\":\""+imei+"\"}]}" ;
			str = str.substring(0,str.length()-2)+son;
		}
		
		TextFileUtil.write(filePath, false, str);		
		
	}

}
