package com.feilo.model;

import com.jfinal.plugin.activerecord.Model;

public class Account extends Model<Account>{

	private static final long serialVersionUID = -8998675668007030427L;
	
	public static final Account dao = new Account();
}
