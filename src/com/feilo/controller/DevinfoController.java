package com.feilo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.feilo.model.Devinfo;
import com.feilo.service.DevinfoService;
import com.jfinal.core.Controller;

public class DevinfoController extends Controller {

	
	/**
	 * 设备号列表页面
	 */
	public void devlist(){
		render("/devlist.html");
	}
	
	/**
	 * 获取设备号列表
	 */
	public void getDevList(){
		String account = getSession().getAttribute("account").toString();
		List<Devinfo> list = Devinfo.dao.find("select * from devinfo where account=?",new Object[]{account});
		Map<String,Object> m = new HashMap<String, Object>();
		m.put("data", list);
		m.put("total", list.size());
		renderJson(m);
	}
	
	/**
	 * 同步设备
	 */
	public void synDevInfo(){
		String account = getSession().getAttribute("account").toString();
		String pwd = getSession().getAttribute("pwd").toString();
		String imeisData = DevinfoService.getDevListForAccount(account,pwd);
		JSONObject jsonObject = new JSONObject(imeisData);
	    JSONArray data =jsonObject.getJSONArray("data");
	    DevinfoService.updateDevinfo(account,data);
	    render("/devlist.html");
	}
	
	
}
