package com.feilo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.feilo.model.MileAge;
import com.feilo.service.DistanceService;
import com.jfinal.core.Controller;

public class MileAgeController extends Controller {

	
	/**
	 * 设备号列表页面
	 */
	public void mileagelist(){
		render("/mileagelist.html");
	}
	
	/**
	 * 获取设备号列表
	 */
	public void getMileAgeList(){
		String account = getSession().getAttribute("account").toString();
		List<MileAge> list = DistanceService.getMileAgeList(account);
		Map<String,Object> m = new HashMap<String, Object>();
		m.put("data", list);
		m.put("total", list.size());
		renderJson(m);
	}
	
	
}
