package com.feilo.controller;

import com.feilo.service.AccountService;
import com.feilo.util.MD5Util;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

public class AccountController extends Controller {
	
	
	/**
	 * 登录界面
	 */
	@Clear
	public void login(){
		render("/login.html");
	}
	
	/**
	 * 登录操作
	 */
	@Clear
	public void doLogin(){
		String account = getPara("account").trim();
		String pwd = getPara("pwd").trim();
		String encrypt = MD5Util.convertMD5(pwd);
		System.out.println(encrypt);
		int id = AccountService.login(account, encrypt);
		if(id>0){
			getSession().setAttribute("id", id);
			getSession().setAttribute("account", account);
			getSession().setAttribute("pwd", pwd);
			render("/devlist.html");
		}else{
			render("/login.html");
		}
	}
	
}
