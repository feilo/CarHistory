package com.feilo.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class AuthInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		System.out.println("interceptor is working...");
		Controller controller = inv.getController();
	    String account = controller.getSessionAttr("account");
	    if (account != null){
	    	inv.invoke();
	    }else{
	      controller.redirect("/nopower.html");
	    }
	}

}
