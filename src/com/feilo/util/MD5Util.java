package com.feilo.util;

import java.security.MessageDigest;
  
/** 
 * 采用MD5加密解密 
 * @author zhangrl 
 * @datetime 2015-09-07 
 */  
public class MD5Util {  
  
    /*** 
     * MD5加码 生成32位md5码 
     */  
    public static String string2MD5(String inStr){  
        MessageDigest md5 = null;  
        try{  
            md5 = MessageDigest.getInstance("MD5");  
        }catch (Exception e){  
            System.out.println(e.toString());  
            e.printStackTrace();  
            return "";  
        }  
        char[] charArray = inStr.toCharArray();  
        byte[] byteArray = new byte[charArray.length];  
  
        for (int i = 0; i < charArray.length; i++)  
            byteArray[i] = (byte) charArray[i];  
        byte[] md5Bytes = md5.digest(byteArray);  
        StringBuffer hexValue = new StringBuffer();  
        for (int i = 0; i < md5Bytes.length; i++){  
            int val = ((int) md5Bytes[i]) & 0xff;  
            if (val < 16)  
                hexValue.append("0");  
            hexValue.append(Integer.toHexString(val));  
        }  
        return hexValue.toString();  
  
    }  
  
    /** 
     * 加密解密算法 执行一次加密，两次解密 
     */   
    public static String convertMD5(String inStr){  
  
        char[] a = inStr.toCharArray();  
        for (int i = 0; i < a.length; i++){  
            a[i] = (char) (a[i] ^ 't');  
        }  
        String s = new String(a);  
        return s;  
  
    }  
    
    /**
     * 两次MD5后进行一次加密
     * @param soure
     * @return
     */
    public static String encrypt(String soure){
		return convertMD5(string2MD5(string2MD5(soure)));
	}
    
    /**
     * 解密后的字符串需要原文两次MD5才能与之匹配
     * @param pwd
     * @return
     */
    public static String decrypt(String pwd){
    	return convertMD5(pwd);
    }
  
    // 测试主函数  
    public static void main(String args[]) {  
        String s = new String("lzgdj4973759"); 
        System.out.println("原字符串：" + s); 
        System.out.println("加密原字符串："+convertMD5(s));
        System.out.println("解密原字符串："+convertMD5(convertMD5(s)));
        System.out.println("1次MD5后：" + string2MD5(s)); 
        System.out.println("两次MD5后：" + string2MD5(string2MD5(s))); 
        System.out.println("两次MD5后并加密：" + convertMD5(string2MD5(string2MD5(s))));
        System.out.println("两次MD5后并解密：" + convertMD5(convertMD5(string2MD5(string2MD5(s)))));
    }  
}
