package com.feilo.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesUtil {
	
	public static String getIMEIString(String key) {
		InputStream inputStream = PropertiesUtil.class.getResourceAsStream("/imei.properties");
		Properties p = new Properties();
		try {
			p.load(inputStream);
			inputStream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return p.getProperty(key);
	}
	
	public static String getDBString(String key) {
		InputStream inputStream = PropertiesUtil.class.getResourceAsStream("/db.properties");
		Properties p = new Properties();
		try {
			p.load(inputStream);
			inputStream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return p.getProperty(key);
	}

}
