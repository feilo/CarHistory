package com.feilo.service;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.feilo.util.BaseUtil;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class DevinfoService {
	
	/**
	 * 获取用户token值
	 * @param account 用户账号
	 * @param pwd     用户密码
	 * @return        返回Token字符串值
	 */
	public static String getDevListForAccount(String account, String pwd) {
		String access_token = GpsHistoryDataService.getToken(account,pwd);
		long time = System.currentTimeMillis() / 1000;
		String url = "http://api.gpsoo.net/1/account/devinfo?target=" + account 
				+ "&account=" + account + "&access_token="+ access_token + "&time=" + time;
		return BaseUtil.getReturnData(url);
	}
	
	public static void saveDevinfo(String account,JSONArray data){
		for (int i = 0; i < data.length(); i++) {
			JSONObject jo = data.getJSONObject(i);
			String sql = "insert into devinfo(imei,name,number,phone,group_id,group_name," +
					"dev_type,in_time,out_time,sudu,efence_support,account) values(?,?,?,?,?,?,?,?,?,?,?,?)" ;
			Db.update(sql, new Object[]{jo.get("imei"),jo.get("name"),jo.get("number"),
					jo.get("phone"),jo.get("group_id"),jo.get("group_name"),jo.get("dev_type"),
					jo.get("in_time"),jo.get("out_time"),jo.get("sudu"),jo.get("efence_support"),account});
	    }
	}
	
	public static void updateDevinfo(String account,JSONArray data){
		for (int i = 0; i < data.length(); i++) {
			JSONObject jo = data.getJSONObject(i);
			String imei = jo.getString("imei");
			String sql = "select imei from devinfo where imei = ? and account =?"  ;
			Record record = Db.findFirst(sql,new Object[]{imei,account});
			if(record==null){
				String addsql = "insert into devinfo(imei,name,number,phone,group_id,group_name," +
						"dev_type,in_time,out_time,sudu,efence_support,account) values(?,?,?,?,?,?,?,?,?,?,?,?)" ;
				Db.update(addsql, new Object[]{jo.get("imei"),jo.get("name"),jo.get("number"),
						jo.get("phone"),jo.get("group_id"),jo.get("group_name"),jo.get("dev_type"),
						jo.get("in_time"),jo.get("out_time"),jo.get("sudu"),jo.get("efence_support"),account});
				System.out.println("insert dev success!");
			}else{
				String addsql = "update devinfo set name=?,number=?,phone=?,group_id=?,group_name=?," +
						"dev_type=?,in_time=?,out_time=?,sudu=?,efence_support=? where imei = ? and account =?" ;
				Db.update(addsql, new Object[]{jo.get("name"),jo.get("number"),jo.get("phone"),jo.get("group_id"),
						jo.get("group_name"),jo.get("dev_type"),jo.get("in_time"),jo.get("out_time"),jo.get("sudu"),
						jo.get("efence_support"),jo.get("imei"),account});
				System.out.println("update dev success!");
			}	
	    }
	}
	
	public static boolean existImei(String value){
		String sql = "select imei from devinfo where imei = ?"  ;
		List<Record> r = Db.find(sql,new Object[]{value});
		if(r.size()>0){
			return true ;
		}else {
			return false ;
		}
	}
	
	public static String[] getImeisFromDevinfo(String account){
		String sql = "select imei from devinfo where account=?";
		List<Record> records = Db.find(sql,new Object[]{account});
		String[] imeis = new String[records.size()];
		for(int i=0;i<records.size();i++){
			imeis[i]=records.get(i).getStr("imei");
		}
		return imeis ;
	}
	
	public static String[] getImeisFromDevinfo(){
		String sql = "select imei from devinfo";
		List<Record> records = Db.find(sql);
		String[] imeis = new String[records.size()];
		for(int i=0;i<records.size();i++){
			imeis[i]=records.get(i).getStr("imei");
		}
		return imeis ;
	}

}
