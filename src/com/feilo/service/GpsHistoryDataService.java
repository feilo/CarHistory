package com.feilo.service;

import org.json.JSONArray;
import org.json.JSONObject;

import com.feilo.util.BaseUtil;
import com.feilo.util.MD5Util;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 获取GPS历史轨迹数据服务
 * @author zhang_rl
 *
 */
public class GpsHistoryDataService {
	
	/**
	 * 获取用户token值
	 * @param account 用户账号
	 * @param pwd     用户密码
	 * @return        返回Token字符串值
	 */
	public static String getToken(String account, String pwd) {
		long time = System.currentTimeMillis() / 1000;
		String signature = MD5Util.string2MD5(MD5Util.string2MD5(pwd) + time);
		String url = "http://api.gpsoo.net/1/auth/access_token?account="
				+ account + "&time=" + time + "&signature=" + signature;
		//System.out.println("获取用户Token的url:"+url);
		String ret = BaseUtil.getReturnData(url);
		//System.out.println("获取用户Token返回值："+ret);
		JSONObject jo = new JSONObject(ret);
		String access_token = jo.get("access_token").toString();
		return access_token;
	}
	
	/**
	 * 批量处理设备号的GPS历史轨迹数据，先通过接口获取GPS历史轨迹数据，并把获取的数据存入数据库
	 * 
	 * @param account    账号
	 * @param pwd        密码
	 * @param time       UNIX时间戳（精确到秒）
	 * @param imeis      IMEI设备ID数组
	 * @param map_type   地图类型
	 * @param begin_time 开始时间（精确到秒）
	 * @param end_time   结束时间（精确到秒）
	 */
	public static void doGpsHistoryData(String account,String pwd,long time,String[] imeis,
			String map_type, long begin_time,long end_time){
		for(String imei:imeis){	
			//根据设备号（imei）获取历史轨迹数据
		    String historyData  = getGpsHistoryData(account, pwd, time, imei, "BAIDU", begin_time, end_time, "1000");
		    JSONObject jsonObject = new JSONObject(historyData);
		    JSONArray data =jsonObject.getJSONArray("data");
		    //处理设备（id）的GPS历史数据
		    doGpsHistoryData(account, pwd, time, imei, map_type, begin_time, end_time, data);
		}
	}
	
	
	/**
	 * 从服务器获取GPS历史轨迹数据，每次最多获取1000条，大于1000条时，
	 * 从第1000条获取时间作为开始时间继续获取，直到获取完成所有数据，并把获取的数据存入数据库
	 * 
	 * @param account    	账号      
	 * @param access_token  token    
	 * @param time       	UNIX时间戳（精确到秒）
	 * @param imei       	IMEI设备ID
	 * @param map_type   	地图类型
	 * @param begin_time 	开始时间（精确到秒）
	 * @param end_time   	结束时间（精确到秒）
	 * @param data       	历史轨迹数据
	 */
	public static void doGpsHistoryData(String account, String access_token, long time,String imei,
			String map_type, long begin_time,long end_time, JSONArray data){
		int length = data.length();
		if(length==0){
			return ;
		}else if(length<1000){
			//把获取的数据存入数据库
			putGpsHistoryDataToDb(imei,data);
			return ;
		}else{
			//把获取的数据存入数据库
			putGpsHistoryDataToDb(imei,data);
			//获取最后一条GPS数据
			JSONObject jo = data.getJSONObject(length-1);
			long begin_t = jo.getLong("gps_time");
			//把最后一条数据的GPS时间作为开始时间继续获取GPS历史轨迹数据，直到获取完所有在数据
			String historyData  = getGpsHistoryData(account, access_token, time, imei, "BAIDU", begin_t, end_time, "1000");
			JSONObject jsonObject = new JSONObject(historyData);
			JSONArray jsonArray =jsonObject.getJSONArray("data");
			//递归调用
			doGpsHistoryData(account, access_token, time, imei, "BAIDU", begin_t, end_time,jsonArray);
		}
	}
	
	/**
	 * 把设备号所对应GPS历史轨迹数据存入数据库
	 * @param imei 设备号
	 * @param data Json数据串
	 */
	public static void putGpsHistoryDataToDb(String imei,JSONArray data){
	    for (int i = 0; i < data.length(); i++) {
			JSONObject jo = data.getJSONObject(i);
			String sql = "insert into gpsdata(imei,gps_time,lng,lat,course,speed) values(?,?,?,?,?,?)" ;
			Db.update(sql, new Object[]{imei,jo.get("gps_time"),jo.get("lng"),jo.get("lat"),jo.get("course"),jo.get("speed")});
	    }
	    //System.out.println("insert ok ");
	}
	
	/**
	 * 通过token来批量处理设备号的GPS历史轨迹数据，先通过接口获取GPS历史轨迹数据，并把获取的数据存入数据库
	 * 
	 * @param access_token  账号
	 * @param account       密码
	 * @param time       	UNIX时间戳（精确到秒）
	 * @param imeis      	IMEI设备ID数组
	 * @param map_type   	地图类型
	 * @param begin_time 	开始时间（精确到秒）
	 * @param end_time   	结束时间（精确到秒）
	 */
	public static void doGpsHistoryDataWithToken(String access_token,String account,long time,String[] imeis,
			String map_type, long begin_time,long end_time){
		for(String imei:imeis){	
			//System.out.println("======开始获取"+imei+"的历史轨迹数据=======");
		    String historyData  = getHistoryWithToken(access_token, account, time, imei, "BAIDU", begin_time, end_time, "1000");
		    JSONObject jsonObject = new JSONObject(historyData);
		    JSONArray data =jsonObject.getJSONArray("data");
		    getDataWithToken(access_token, account, time, imei, map_type, begin_time, end_time, data);
		}
	}
	
	/**
	 * 从服务器获取GPS历史轨迹数据，每次最多获取1000条，大于1000条时，
	 * 从第1000条获取时间作为开始时间继续获取，直到获取完成所有数据。
	 * 
	 * @param access_token  token值  
	 * @param account    	账号    
	 * @param time       	UNIX时间戳（精确到秒）
	 * @param imei       	IMEI设备ID
	 * @param map_type   	地图类型
	 * @param begin_time 	开始时间（精确到秒）
	 * @param end_time   	结束时间（精确到秒）
	 * @param data       	历史轨迹数据
	 */
	public static void getDataWithToken(String access_token, String account, long time,String imei,
			String map_type, long begin_time,long end_time, JSONArray data){
		int length = data.length();
		if(length==0){
			return ;
		}else if(length<1000){
			putGpsHistoryDataToDb(imei,data);
			return ;
		}else{
			putGpsHistoryDataToDb(imei,data);
			JSONObject jo = data.getJSONObject(length-1);
			long begin_t = jo.getLong("gps_time");
			String historyData  = getHistoryWithToken(access_token, account, time, imei, "BAIDU", begin_t, end_time, "1000");
			JSONObject jsonObject = new JSONObject(historyData);
			JSONArray jsonArray =jsonObject.getJSONArray("data");
			getDataWithToken(access_token, account, time, imei, "BAIDU", begin_t, end_time,jsonArray);
			jsonObject = null ;
			jsonArray  = null ;
		}
	}

	/**
	 * 根据用户、密码和设备号（imei）获取历史轨迹数据
	 * @param account    账号      
	 * @param pwd        密码    
	 * @param time       UNIX时间戳（精确到秒）
	 * @param imei       IMEI设备ID
	 * @param map_type   地图类型
	 * @param begin_time 开始时间（精确到秒）
	 * @param end_time   结束时间（精确到秒）
	 * @param limit		   每次获取GPS数据的条数，最大为1000
	 * @return  json格式的GPS历史轨迹数据
	 */
	public static String getGpsHistoryData(String account,String access_token,long time, String imei, 
			String map_type, long begin_time,long end_time, String limit) {
		String url = "http://api.gpsoo.net/1/devices/history?access_token="
				+ access_token + "&account=" + account + "&time=" + time
				+ "&imei=" + imei + "&map_type=" + map_type + "&begin_time="
				+ begin_time + "&end_time=" + end_time + "&limit=" + limit;
		//System.out.println("获取历史轨迹数据url："+url);
		return BaseUtil.getReturnData(url);
	}
	

	/**
	 * 根据access_token、用户和设备号（imei）获取历史轨迹数据
	 * @param access_token  token值      
	 * @param account       账号    
	 * @param time       	UNIX时间戳（精确到秒）
	 * @param imei       	IMEI设备ID
	 * @param map_type   	地图类型
	 * @param begin_time 	开始时间（精确到秒）
	 * @param end_time   	结束时间（精确到秒）
	 * @param limit		   	每次获取GPS数据的条数，最大为1000
	 * @return  json格式的GPS历史轨迹数据
	 */
	public static String getHistoryWithToken(String access_token, String account,long time, 
			String imei, String map_type, long begin_time,long end_time, String limit) {
		String url = "http://api.gpsoo.net/1/devices/history?access_token="
				+ access_token + "&account=" + account + "&time=" + time
				+ "&imei=" + imei + "&map_type=" + map_type + "&begin_time="
				+ begin_time + "&end_time=" + end_time + "&limit=" + limit;
		return BaseUtil.getReturnData(url);
	}
	
	public static int getEndTime(String imei){
		String sql = "select MAX(gps_time) as endtime from gpsdata where imei=?" ;
		Record r = Db.findFirst(sql,new Object[]{imei});
		if(r.get("endtime")==null){
			return -1 ;
		}else{
			return r.getInt("endtime");
		}
	}

	public static String getHistoryData(String access_token, String account,
			long time, String imei, String map_type, long begin_time,long end_time) {
		String ret = getHistoryWithToken(access_token, account, time, imei, "BAIDU",begin_time, end_time, "1000");
		JSONObject jo = new JSONObject(ret);
		String code = jo.get("ret").toString();
		if (!code.equals("0")) {
			return jo.get("msg").toString();
		} else {
			return jo.get("data").toString();
		}
	}
}
