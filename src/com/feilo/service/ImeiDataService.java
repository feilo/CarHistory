package com.feilo.service;

import java.util.Date;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class ImeiDataService {
	
	public static List<Record> getImeisFromImeiData(int account){
		String sql = "select a.imei from imeidata a inner join accountimei b on a.id=b.imei where b.account=?" ;
    	List<Record> records = Db.find(sql,new Object[]{account});
    	return records ;
	}
	
	public static int insertImeiToImeiData(String imei){
	    String sql = "insert into imeidata(imei) values(?)" ;	    
		Db.update(sql, new Object[]{imei});
		sql = "select id from imeidata where imei=?" ;
		Record r = Db.findFirst(sql, new Object[]{imei});
		return r.getInt("id");
	}

	/**
	 * 根据imei获取imei最后获取GPS轨迹在时间
	 * @param imei
	 * @return
	 */
	public static long getEndTime(String imei) {
		long begin_time = 0 ;
		String sql = "select endtime from imeidata where imei=?";
		Record r = Db.findFirst(sql, new Object[]{imei});
		if(r.getDate("endtime") != null){
			begin_time = r.getDate("endtime").getTime();
		}		
		return begin_time;
	}

	public static void updateEndTime(String imei) {
		String sql = "update imeidata set endtime=? where imei=?" ;	  
		Db.update(sql, new Object[]{new Date(),imei});		
	}
}
