package com.feilo.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class AccountService {
	
	public static List<Record> getAccounts() {
		List<Record> accounts = new ArrayList<Record>();
		String sql = "select * from account";
		accounts = Db.find(sql);
		return accounts;
	}

	public static int login(String account,String pwd){
		String sql = "select * from account where account=? and pwd=?";
		List<Record> data = Db.find(sql,new Object[]{account,pwd});
		if(data.size()>0){
			return data.get(0).getInt("id") ;
		}
		return -1 ;
	}
	
	public static List<Record> getImeisFromAccount(String account){
		String sql = "select imei from devinfo where account=?" ;
    	List<Record> records = Db.find(sql,new Object[]{account});
    	return records ;
	}
	
	public static void insertImeiToAccountImei(int account,int imei){
		String sql = "insert into accountimei(account,imei) values(?,?)" ;
		Db.update(sql, new Object[]{account,imei});
	}
}
