package com.feilo.job;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.feilo.service.AccountService;
import com.feilo.service.GpsHistoryDataService;
import com.feilo.util.MD5Util;
import com.jfinal.plugin.activerecord.Record;

/**
 * quartz 工作类
 * @author zhangrl
 *
 */
public class GpsHisttoryDataJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("GpsHisttoryDataJob is woiking...");
		//获取用户列表
		List<Record> accounts = getAccounts();		
		for(Record record : accounts){
			String account = record.getStr("account");
			String sourcePwd = record.getStr("pwd");
			String aimPwd = MD5Util.convertMD5(sourcePwd);
			String access_token = GpsHistoryDataService.getToken(account,aimPwd);
			//System.out.println(new Date()+":=======正在初始化用户"+account+"下所有设备的GPS轨迹数据======||"+sourcePwd+"||"+aimPwd);
			//根据用户ID获取设备号列表
			List<Record> records = getImeis(account);
			for(Record imei : records){
				//System.out.println(new Date()+":=======正在初始化设备"+imei.getStr("imei")+"的GPS轨迹数据======");
				work(account, access_token, imei.getStr("imei"));
			}
		}
	}
	
	/**
	 * 根据用户ID获取设备号列表
	 * @param account 用户ID
	 * @return 设备号列表
	 */
	private List<Record> getImeis(String account) {
		List<Record> records = AccountService.getImeisFromAccount(account);
		return records;
	}

	/**
	 * 获取取用户列表
	 * @return 取用户列表
	 */
	private List<Record> getAccounts() {
		List<Record> accounts = AccountService.getAccounts();
		return accounts;
	}


	/**
	 * 获取设备的历史轨迹，并存入数据库
	 * @param account
	 * @param pwd
	 * @param imei
	 */
	private void work(String account,String access_token,String imei){
		long time = System.currentTimeMillis()/1000;
		//获取指定设备号（imei）最后获取GPS历史轨迹数据的最后时间为开始时间
		long endtime = GpsHistoryDataService.getEndTime(imei);
		//如果最后时间为空，则取当前到3个月前的数据
		if(endtime<0){
			Date date = new Date();//当前日期
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");//格式化对象
			Calendar calendar = Calendar.getInstance();//日历对象
			calendar.setTime(date);//设置当前日期
			calendar.add(Calendar.MONTH, -3);//月份减3
			String format = sdf.format(calendar.getTime());
			try {
				endtime = sdf.parse(format).getTime()/1000;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		//获取设备号的GPS历史数据json串
	    String historyData  = GpsHistoryDataService.getGpsHistoryData(account, access_token, time, imei, "BAIDU", endtime, time, "1000");
	    //System.out.println("historyData="+historyData);
	    JSONObject jsonObject = new JSONObject(historyData);
	    JSONArray data =jsonObject.getJSONArray("data");
	    //处理所获取的GPS数据
	    if(data.length()>0){	    
	    	GpsHistoryDataService.doGpsHistoryData(account, access_token, time, imei, "BAIDU", endtime, time, data);
	    }
	}

}
